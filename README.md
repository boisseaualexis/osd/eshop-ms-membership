# Guide for eshop-ms-membership

## Pull from DockerHub


```bash
docker pull episenba/eshop-ms-membership:1.0.0-RELEASE
```

## Docker commands
Run container
```bash
docker run -d -p 8070:8070 --name ms-membership eshop-ms-membership:1.0.0-RELEASE
```
Check logs
```bash
docker logs ms-membership
```
Stop container
```bash
docker stop ms-membership
```
Remove container
```bash
docker container rm ms-membership
```
## Logs

**Check logs to monitor server execution and see actions like *JWToken* generation and validation or management of the member repository.**

## Usage

####Authenticate

> POST request

http://localhost:8070/episen/osd/membership/api/v1/authenticate
> BODY
```json
{
	"login": "XXX",
	"password": "XXX"
}
```
> Check logs for members **login/password** created at initialisation or see section *####See all members* of this file or :
>
> Use **admin/admin** for ADMIN authority
>
> Use **bob/bob** for USER authority
> 
> Check logs or http response to retrieve the JWT ***token*** created at authentication.
>
> The ***token*** contains the member's authorities.

----------
####Create member : only with ADMIN authority

> POST request

http://localhost:8070/episen/osd/membership/api/v1/members
> BODY
```json
{
	"token": "XXX.XXX.XXX",
	"username": "XXX",
	"password": "XXX",
	"age": XX,
	"email": "XXX",
	"authorities": ["XXX", "XXX"]
}
```
> Only `token` and `username` fields are required but a non-null `password` is required for authentication.


----------
####See all members
> GET request

http://localhost:8070/episen/osd/membership/api/v1/members

----------
####See one member
> GET request

http://localhost:8070/episen/osd/membership/api/v1/members/{username}

----------
####Update member

> PUT request

http://localhost:8070/episen/osd/membership/api/v1/members/{username}
> BODY
```json
{
	"username": "XXX",
	"password": "XXX",
	"age": XX,
	"email": "XXX",
	"authorities": ["XXX", "XXX"]
}
```
> Will update the member corresponding to {username} in URL.
>
> All fields are optionals.

----------
####Delete member

> DELETE request

http://localhost:8070/episen/osd/membership/api/v1/members/{username}

> Will delete the member corresponding to {username} in URL.



