package org.episen.osd.membership;

import org.episen.osd.membership.security.JWTGenerator;
import org.episen.osd.membership.service.MemberService;
import org.episen.osd.membership.settings.PopulateMemberRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class EshopMsMembershipApplication implements CommandLineRunner {

	@Autowired
	private JWTGenerator generator;

	@Autowired
	private MemberService service;

	public static void main(String[] args) {

		SpringApplication.run(EshopMsMembershipApplication.class, args);
		System.out.println("MEMBERSHIP MICROSERVICE STARTED");
	}

	@Override
	public void run(String... args) {
		System.out.println("MEMBERSHIP MICROSERVICE INITIALISATION");

		PopulateMemberRepo populateMemberRepo = new PopulateMemberRepo(service, "/json/members.json");
		System.out.println("POPULATING MEMBERS IN MEMORY");
		populateMemberRepo.populate();
		System.out.println("DONE POPULATING MEMBERS IN MEMORY");

	}

}
