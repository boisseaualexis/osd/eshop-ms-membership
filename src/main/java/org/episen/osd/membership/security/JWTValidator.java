package org.episen.osd.membership.security;

import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.episen.osd.membership.model.UserContext;
import org.episen.osd.membership.settings.InfraSettings;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;

@Service
public class JWTValidator {

	private JWSVerifier verifier;
	
	@PostConstruct
	public void init(){
		
		verifier = new RSASSAVerifier((RSAPublicKey) InfraSettings.keyPairLoader().getPublic());
	}
	
	public UserContext transform(String token){
		
		try {
			
			SignedJWT signedJwt = SignedJWT.parse(token);
			if(!signedJwt.verify(verifier)){
				throw new RuntimeException("Token cannot be verified. Invalid Token : Wrong Signature");
			}
			
			if(!validate(signedJwt.getJWTClaimsSet())){
				throw new RuntimeException("Token cannot be verified. Invalid Token : Expired or Wrong Issuer");
			}
			System.out.println("TOKEN VALIDATED");

			UserContext userContext = new UserContext();
			userContext.setSubject(signedJwt.getJWTClaimsSet().getSubject());
			userContext.setAuthorities(signedJwt.getJWTClaimsSet().getStringListClaim("AUTHORITIES"));
			
			return userContext;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private boolean validate(JWTClaimsSet claims){
		System.out.println("VALIDATING TOKEN");
		return validateTokenExpiration(claims) && validateTokenIssuer(claims);
	}

	private boolean validateTokenExpiration(JWTClaimsSet claims){
		boolean expiration = Instant.now().isBefore(claims.getExpirationTime().toInstant());
		System.out.println("Checking token expiration : "+ expiration);
		return expiration;
	}

	private boolean validateTokenIssuer(JWTClaimsSet claims){
		boolean issuer = claims.getIssuer().equalsIgnoreCase("ESHOP-MEMBERSHIP");
		System.out.println("Checking token issuer : "+ issuer);
		return issuer;
	}
}
