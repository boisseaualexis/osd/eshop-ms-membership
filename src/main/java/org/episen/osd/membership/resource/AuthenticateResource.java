package org.episen.osd.membership.resource;

import org.episen.osd.membership.model.AuthenticateModel;
import org.episen.osd.membership.model.MemberModel;
import org.episen.osd.membership.security.JWTGenerator;
import org.episen.osd.membership.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.util.xml.StaxUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
@RequestMapping(value = "authenticate", produces = {"application/json"})
public class AuthenticateResource {

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    private MemberService service;


    @PostMapping
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public String authenticate(@RequestBody AuthenticateModel authModel) {

        if (authModel == null
                || authModel.getPassword().isBlank() || authModel.getPassword() == null
                || authModel.getLogin().isBlank() || authModel.getLogin() == null) {
            return "Error : Authentication format not valid";
        }

        MemberModel member = service.getMember(authModel.getLogin());

        if (member == null) {
            return "Error : Member "+authModel.getLogin()+" does not exist";
        }

        if (!member.getPassword().equals(authModel.getPassword())) {
            return "Error : Member "+authModel.getLogin()+" : Wrong Password";
        }

        if (member.getAuthorities().contains("USER")) {

            String token = jwtGenerator.generateToken(authModel.getLogin(), member.getAuthorities());

            /*String urlRedirect = "http://localhost:8060//episen/osd/basket/api/v1/authenticate/validate";     // Tentative de communiquer avec ms-basket mais les 2 microservices ne sont pas en réseau
            ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(urlRedirect, token, String.class);*/

            //System.out.println(responseEntityStr.getStatusCode());
            //System.out.println(responseEntityStr.getBody());

            String newLine = System.getProperty("line.separator");
            String stringInfo = "Token for " + authModel.getLogin() + " : " + token + newLine
                    + "Authentication and basket creation for " + authModel.getLogin() + " at http://localhost:8060/episen/osd/basket/api/v1/authenticate/validate" + newLine
                    + "Basket available for " + authModel.getLogin() + " at http://localhost:8060/episen/osd/basket/api/v1/baskets/" + authModel.getLogin() + newLine
                    + "Member management available for " + authModel.getLogin() + " at http://localhost:8070/episen/osd/membership/api/v1/members/"+authModel.getLogin();
            System.out.println(stringInfo);

            return stringInfo;
            //return "Basket microservice response status : " + responseEntityStr.getStatusCode() + ", response body : " + responseEntityStr.getBody();
        }

        if (member.getAuthorities().contains("ADMIN")) {
            String token = jwtGenerator.generateToken(authModel.getLogin(), member.getAuthorities());

            String newLine = System.getProperty("line.separator");
            String stringInfo = "Token for " + authModel.getLogin() + " : " + token + newLine
                    + "Member creation and management available for " + authModel.getLogin() + " at http://localhost:8070/episen/osd/membership/api/v1/members/";
            System.out.println(stringInfo);

            return stringInfo;
        }

        return "Error : Member "+authModel.getLogin()+" authorities not accepted : "+member.getAuthorities();
    }
}