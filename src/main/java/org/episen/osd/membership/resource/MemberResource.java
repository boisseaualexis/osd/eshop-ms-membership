package org.episen.osd.membership.resource;

import org.episen.osd.membership.model.MemberCreationContext;
import org.episen.osd.membership.model.MemberModel;
import org.episen.osd.membership.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "members", produces = {"application/json"})
public class MemberResource {

    @Autowired
    private MemberService service;

    @GetMapping
    public List<MemberModel> getAll(){
        return service.getAll();
    }

    @GetMapping("{username}")
    public MemberModel getOne(@PathVariable("username") String username){

        return service.getMember(username);
    }

    @PostMapping()
    public void addMember(@RequestBody MemberCreationContext member){

        service.addMember(member);
    }

    @PutMapping("{username}")
    public void updateMember(@PathVariable("username") String username, @RequestBody MemberModel member){

        service.updateMember(username, member);
    }

    @DeleteMapping("{username}")
    public void deleteMember(@PathVariable("username") String username){

        service.deleteMember(username);
    }
}
