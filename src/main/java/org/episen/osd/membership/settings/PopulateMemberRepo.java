package org.episen.osd.membership.settings;

import org.episen.osd.membership.model.MemberCreationContext;
import org.episen.osd.membership.service.MemberService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PopulateMemberRepo {

    private String filePath;

    private JSONParser jsonParser;

    private MemberService service;

    public PopulateMemberRepo(MemberService service, String filePath) {
        this.filePath = filePath;
        this.jsonParser = new JSONParser();
        this.service = service;
    }

    public void populate(){

        try {
            InputStream is = PopulateMemberRepo.class.getResourceAsStream(this.filePath);
            //System.out.println(is.available());
            //File file = resource.getFile();
            //FileReader reader = new FileReader("/json/members.json");

            Object obj = new JSONParser().parse(new InputStreamReader(is, "UTF-8"));
            JSONArray memberList = (JSONArray) obj;

            //Iterate over member array
            memberList.forEach( member -> parseMemberModel( (JSONObject) member ) );

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

    }

    private void parseMemberModel(JSONObject member)
    {
        MemberCreationContext memberToAdd = new MemberCreationContext();

        //Get member username
        String username = (String) member.get("username");
        memberToAdd.setUsername(username);

        //Get member password
        String password = (String) member.get("password");
        memberToAdd.setPassword(password);

        //Get member email
        String email = (String) member.get("email");
        memberToAdd.setEmail(email);

        //Get member age
        Long ageLong = (Long) member.get("age");
        memberToAdd.setAge(ageLong.intValue());

        //Get member authorities
        List<String> authorities = (ArrayList<String>) member.get("authorities"); //new ArrayList<>();
        /*JSONArray authoritiesList = (JSONArray) member.get("authorities");
            //Iterate over member authorities array
        authoritiesList.forEach( auth -> authoritiesList.add(auth) );*/
        memberToAdd.setAuthorities(authorities);

        memberToAdd.setToken("INIT");

        service.addMember(memberToAdd);
    }
}
