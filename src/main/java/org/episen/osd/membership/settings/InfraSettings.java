package org.episen.osd.membership.settings;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;

public class InfraSettings {

    public static KeyPair keyPairLoader(){

        try(InputStream is = InfraSettings.class.getResourceAsStream("/keys/server.p12")) {

            KeyStore kStore = KeyStore.getInstance("PKCS12");
            kStore.load(is, "eshopms".toCharArray());

            Key key = kStore.getKey("eshopms", "eshopms".toCharArray());

            Certificate certificate = kStore.getCertificate("eshopms");

            return new KeyPair(certificate.getPublicKey(), (PrivateKey)key);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
