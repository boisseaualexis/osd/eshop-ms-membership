package org.episen.osd.membership.service;

import org.episen.osd.membership.model.MemberCreationContext;
import org.episen.osd.membership.model.MemberModel;
import org.episen.osd.membership.model.UserContext;
import org.episen.osd.membership.repository.MemberRepository;
import org.episen.osd.membership.security.JWTValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService {

    @Autowired
    private JWTValidator jwtValidator;

    @Autowired
    private MemberRepository repository;

    public void addMember(MemberCreationContext memberToAdd){

        if((null==(memberToAdd.getUsername())) || (memberToAdd.getUsername().isBlank())){
            throw new RuntimeException("Error : Member not added : Requires username");
        }
        if((null==(memberToAdd.getPassword())) || (memberToAdd.getPassword().isBlank())){
            throw new RuntimeException("Error : Member not added : Requires password");
        }
        if(repository.getMemberByUsername(memberToAdd.getUsername())!=null){
            throw new RuntimeException("Exception : Member already exist");
        }

        if((memberToAdd.getToken()==null) || (memberToAdd.getToken().isBlank())){
            throw new RuntimeException("Exception : Member not added : Requires token with 'ADMIN' authority");
        }

        if (!memberToAdd.getToken().equals("INIT")) {

            UserContext userCreator = jwtValidator.transform(memberToAdd.getToken());
            String username = userCreator.getSubject();
            List<String> authorities = userCreator.getAuthorities();

            if (username.isBlank() || (username == null)
                    || (authorities.isEmpty()) || (authorities == null)
                    || !(authorities.contains("ADMIN"))) {
                throw new RuntimeException("Exception : Member not allowed to create member");
            }
        }
        MemberModel member = new MemberModel(memberToAdd);
        repository.addMember(member);
    }

    public MemberModel getMember(String username){

        MemberModel member = repository.getMemberByUsername(username);

        if(member == null){
            throw new RuntimeException("Get Exception : Member not found");
        }

        return member;
    }

    public List<MemberModel> getAll(){
        return repository.getAll();
    }

    public void updateMember(String username, MemberModel member){

        if(repository.getMemberByUsername(username) == null) {
            throw new RuntimeException("Update Error : Member not found");
        }
        if(member.getUsername() == null) {
            member.setUsername(username);
        }

        repository.updateMember(member);
    }

    public void deleteMember(String username){

        if(repository.getMemberByUsername(username) == null){
            throw new RuntimeException("Delete Error : Member not found");
        }

        repository.deleteMemberByUsername(username);
    }

    public MemberRepository getRepository() {
        return repository;
    }

    public void setRepository(MemberRepository repository) {
        this.repository = repository;
    }
}
