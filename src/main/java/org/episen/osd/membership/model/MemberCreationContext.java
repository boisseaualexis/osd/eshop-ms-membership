package org.episen.osd.membership.model;

import java.util.List;

public class MemberCreationContext extends MemberModel {

    public MemberCreationContext(MemberCreationContext memberToAdd) {
        super(memberToAdd);
        this.token = memberToAdd.getToken();
    }

    public MemberCreationContext() {
        super();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;
}
