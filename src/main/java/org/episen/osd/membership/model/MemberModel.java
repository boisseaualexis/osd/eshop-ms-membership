package org.episen.osd.membership.model;

import java.util.List;

public class MemberModel {

    private String username;

    private String password;

    private List<String> authorities;

    private String email;

    private int age;

    public MemberModel(MemberCreationContext memberToAdd) {
        this.username = memberToAdd.getUsername();
        this.password = memberToAdd.getPassword();
        this.authorities = memberToAdd.getAuthorities();
        this.email = memberToAdd.getEmail();
        this.age = memberToAdd.getAge();
    }

    public MemberModel() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "username='" + username + '\'' +
                ", authorities='" + authorities + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
