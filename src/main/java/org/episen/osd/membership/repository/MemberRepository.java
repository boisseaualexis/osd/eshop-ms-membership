package org.episen.osd.membership.repository;

import org.episen.osd.membership.model.MemberModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MemberRepository {
    private Map<String, MemberModel> memberInMemory = new HashMap<>();

    public void addMember(MemberModel member){
        System.out.println("AddMember , username :" + member.getUsername());
        memberInMemory.put(member.getUsername(), member);
    }

    public MemberModel getMemberByUsername(String username){
        System.out.println("GetMemberByUsername , username : " + username);
        return memberInMemory.get(username);
    }

    public List<MemberModel> getAll(){
        return new ArrayList<MemberModel>(memberInMemory.values());
    }

    public void updateMember(MemberModel member){
        System.out.println("UpdateMember , username : " + member.getUsername());
        memberInMemory.put(member.getUsername(), member);
    }

    public void deleteMemberByUsername(String username){
        System.out.println("DeleteMemberByUsername , username : " + username);
        memberInMemory.remove(username);
    }
}
