FROM openjdk:15-alpine

VOLUME /tmp

COPY target/eshop-ms-membership-1.0.0-RELEASE.jar app.jar

CMD ["java", "-jar", "/app.jar"]

EXPOSE 8070